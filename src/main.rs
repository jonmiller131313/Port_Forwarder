use std::time::Duration;

use anyhow::{anyhow, Error, Result};
use clap::Parser;
use humantime::format_duration;
use igd_next::{search_gateway, SearchError, SearchOptions};
use local_ip_address::local_ip;

use crate::cli::CliArgs;

mod cli;
mod port;

fn main() -> Result<()> {
    let args = CliArgs::parse();
    eprintln!("[?] Port lease duration: {}", format_duration(args.duration));

    let ip = local_ip().map_err(|e| {
        eprintln!("[!] Unable to get computer's LAN IP address:");
        e
    })?;
    eprintln!("[?] Local IP address: {ip}");

    let gateway = search_gateway(SearchOptions {
        timeout: Some(Duration::from_secs(args.timeout)),
        ..SearchOptions::default()
    })
    .map_err(check_upnp_disabled)?;
    eprintln!("[?] Router's local IP: {}", gateway.addr.ip());

    match gateway.get_external_ip() {
        Ok(external_ip) => eprintln!("[?] External IP: {external_ip}"),
        Err(e) => {
            eprintln!("[!] Unable to get external IP: {e}");
            return Err(anyhow!(e));
        }
    }

    eprintln!("[-] Adding ports ...");
    for port in args.ports {
        if let Err(e) = port.open(&gateway, ip, &args.duration, &args.description) {
            eprintln!("   [!] {e}");
        } else {
            eprintln!("   [-] added port(s): {port}");
        }
    }

    Ok(())
}

fn check_upnp_disabled(error: SearchError) -> Error {
    use std::io::ErrorKind::WouldBlock;

    eprint!("[!] Unable to get router's local IP: ");
    match &error {
        SearchError::IoError(e) if e.kind() == WouldBlock => eprintln!("(is UPnP enabled on your router?)"),
        _ => eprintln!(),
    }

    anyhow!(error)
}
