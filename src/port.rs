use std::{
    fmt,
    net::{IpAddr, SocketAddr},
    num::NonZeroU16,
    ops::RangeInclusive,
    str::FromStr,
    time::Duration,
};

use anyhow::{anyhow, bail, ensure, Context, Error, Result};
use igd_next::{
    Gateway,
    PortMappingProtocol::{TCP, UDP},
};
use once_cell::sync::Lazy;
use regex::Regex;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Protocol {
    Both,
    Tcp,
    Udp,
}
impl TryFrom<char> for Protocol {
    type Error = Error;

    fn try_from(value: char) -> Result<Self> {
        Ok(match value.to_uppercase().next() {
            Some('B') => Self::Both,
            Some('T') => Self::Tcp,
            Some('U') => Self::Udp,
            _ => bail!("port protocol must be either 'B', 'T', or 'U'"),
        })
    }
}
impl fmt::Display for Protocol {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Protocol::*;
        write!(
            f,
            "{}",
            match self {
                Both => "BOTH",
                Tcp => "TCP",
                Udp => "UDP",
            }
        )
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Port {
    Single(Protocol, NonZeroU16),
    Range(Protocol, RangeInclusive<NonZeroU16>),
}
impl Port {
    pub fn open(&self, gateway: &Gateway, local_ip: IpAddr, duration: &Duration, desc: &str) -> Result<()> {
        let (&protocol, ports) = match self {
            Self::Single(protocol, port) => (protocol, port.get()..=port.get()),
            Self::Range(protocol, ports) => {
                let (start, end) = ports.clone().into_inner();
                (protocol, start.get()..=end.get())
            }
        };

        for port in ports {
            let socket_addr = SocketAddr::new(local_ip, port);
            let protocol = match protocol {
                Protocol::Tcp => TCP,
                Protocol::Udp => UDP,
                Protocol::Both => {
                    gateway
                        .add_port(TCP, port, socket_addr, duration.as_secs() as _, desc)
                        .map_err(|e| anyhow!("unable to add port {port}: {e}"))?;
                    UDP
                }
            };
            gateway
                .add_port(protocol, port, socket_addr, duration.as_secs() as _, desc)
                .map_err(|e| anyhow!("unable to add port {port}: {e}"))?;
        }

        Ok(())
    }

    fn unpack(&self) -> (Protocol, NonZeroU16, Option<NonZeroU16>) {
        match self {
            Self::Single(protocol, port) => (*protocol, *port, None),
            Self::Range(protocol, range) => {
                let (start, end) = range.clone().into_inner();
                (*protocol, start, Some(end))
            }
        }
    }
}
impl FromStr for Port {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        const ERROR_MSG: &str = "invalid port format, must be YAAAA[-BBBB] (see --help)";
        static RE: Lazy<Regex> =
            Lazy::new(|| Regex::new(r"(?P<type>[BTUbtu])(?P<port>\d{1,5})(?:-(?P<end>\d{1,5}))?").unwrap());

        let parsed = RE.captures(s).context(ERROR_MSG)?;

        let protocol = Protocol::try_from(parsed["type"].as_bytes()[0] as char).unwrap();
        let first_port = parsed["port"].parse().context(ERROR_MSG)?;

        Ok(match parsed.name("end") {
            None => Self::Single(protocol, first_port),
            Some(end) => {
                let end = end.as_str().parse().context(ERROR_MSG)?;
                ensure!(first_port <= end, "port range must be increasing order");
                Self::Range(protocol, first_port..=end)
            }
        })
    }
}
impl fmt::Display for Port {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let (protocol, port, end) = self.unpack();
        let port = port.get();
        let end = end.map(NonZeroU16::get);

        write!(f, "{protocol}/{port}")?;
        if let Some(end) = end {
            write!(f, "-{end}")?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_protocols() {
        use Protocol::*;
        assert_eq!(Protocol::try_from('B').unwrap(), Both);
        assert_eq!(Protocol::try_from('t').unwrap(), Tcp);
        assert_eq!(Protocol::try_from('U').unwrap(), Udp);

        assert!(Protocol::try_from('a').is_err());
    }

    #[test]
    fn test_single_port() {
        use Port::*;
        use Protocol::*;

        let nonzero = |n| NonZeroU16::new(n).unwrap();

        assert_eq!(Port::from_str("T1").unwrap(), Single(Tcp, nonzero(1)));
        assert_eq!(Port::from_str("B150").unwrap(), Single(Both, nonzero(150)));

        assert_eq!(Port::from_str("U10-15").unwrap(), Range(Udp, nonzero(10)..=nonzero(15)));

        assert!(Port::from_str("a500").is_err());
        assert!(Port::from_str("B65536").is_err());
        assert!(Port::from_str("U10-5").is_err());
    }
}
