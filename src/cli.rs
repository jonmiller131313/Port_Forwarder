use std::{str::FromStr, time::Duration};

use anyhow::{ensure, Result};
use clap::Parser;
use humantime::{format_duration, parse_duration as _parse_duration};

use crate::port::Port;

/// Opens up a list of ports on your router (if supported/enabled)
///
/// Ports are passed to the router 1 to 1, no custom port mapping is supported at the moment (e.g. mapping router port
/// 7000 to 4500 on your PC).
///
/// All ranges described here are inclusive.
#[derive(Debug, Parser)]
#[command(version, about)]
pub struct CliArgs {
    /// Duration of the port forwarding lease.
    ///
    /// Examples: 1m (1 minute), 5h or 5hr (5 hours), 1h30m (1 hour 30 minutes). Long formats are also supported ("1
    /// minute 30 seconds").
    ///
    /// See the documentation for humantime::parse_duration() for a list of supported format codes (sub-second
    /// resolution not supported).
    ///
    /// https://docs.rs/humantime/latest/humantime/fn.parse_duration.html
    #[arg(short, long, value_parser = parse_duration, default_value = "4h", value_name = "XXhYYm")]
    pub duration: Duration,

    /// Description sent to the router
    #[arg(long, default_value = "", value_name = "DESC")]
    pub description: String,

    /// How long to wait (in seconds) while searching for the router.
    #[arg(long, default_value = "5")]
    pub timeout: u64,

    /// List of port specifications, see --help for details
    ///
    /// Specific Port:
    /// Specify a specific port by using the format YPPPP where Y is either:
    ///  - B(oth)
    ///  - T(CP)
    ///  - U(DP)
    /// and PPPP is some integer in the range of 1-65535.
    ///
    /// Example: B1025 for both TCP and UDP on port 1025
    ///
    ///
    /// Range of Ports:
    /// You can also give a range of ports by using the format YAAAA-BBBB which
    /// follows a similar format for singular ports but omits the type of the end
    /// port (the entire range uses the same type).
    ///
    /// Example: T100-150 for ports 100 through 150 as TCP
    #[arg(verbatim_doc_comment, required = true, value_delimiter = ' ', value_parser = Port::from_str)]
    pub ports: Vec<Port>,
}

fn parse_duration(s: &str) -> Result<Duration> {
    const MAX_DURATION: Duration = Duration::from_secs(u32::MAX as _);
    let duration = _parse_duration(s)?;
    ensure!(duration.subsec_nanos() == 0, "sub-second values are not supported");
    ensure!(duration.as_secs() > 0, "duration must not be zero");
    ensure!(
        duration.as_secs() <= u32::MAX as _,
        "duration too long, max supported duration: {}",
        format_duration(MAX_DURATION)
    );
    Ok(duration)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_duration_parse() {
        let d = |m: u64| Duration::from_secs(m * 60);

        assert_eq!(parse_duration("15m").unwrap(), d(15));
        assert_eq!(parse_duration("1h80m").unwrap(), d(60 + 80));

        assert!(parse_duration("1").is_err());
        assert!(parse_duration("0m").is_err());
        assert!(parse_duration("100ms").is_err());
    }
}
